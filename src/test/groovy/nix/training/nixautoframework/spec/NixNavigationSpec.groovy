package nix.training.nixautoframework.spec

import geb.spock.GebReportingSpec
import nix.training.nixautoframework.page.StartPage
import nix.training.nixautoframework.page.BlogPage

class NixNavigationSpec extends GebReportingSpec {

    def "Navigate to Blog page"(){
        when: "User at Start page"
            to StartPage

        and: "User navigates to the Blog page"
            "User clicks on Blog page"()

        then: "User at the Blog page"
            at BlogPage
    }
}